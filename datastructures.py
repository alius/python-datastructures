#!/usr/bin/env python

"""
Copyright 2013 Future Industries Inc. http://infuturewetrust.com

Author: Artjom Vassiljev

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and 
limitations under the License.



What is implemented:
    Stack
    Queue
    Dequeue
    LinkedList
    DoublyLinkedList
    CyclicLinkedList - TODO
"""


""" A Stack data structure that is using list for its implementation """
class Stack:

    def __init__(self):
        self.container = []

    def pop(self):
        """ Removes the top element from the stack and returns it """
        if self.container:
            return self.container.pop()
        else:
            raise AttributeError("Stack is empty")

    def push(self, item):
        """ Puts an element on top of the stack """
        self.container.append(item)

    def isEmpty(self):
        """ Tells whether the stack is empty """
        return self.container == []

    def peek(self):
        """ Returns the top element of the stack without removing it """
        return self.container[len(self.container) - 1]

    def clear(self):
        """ Clears the stack """
        self.container = []

    def size(self):
        """ Returns the size of the stack """
        return len(self.container)


""" Implementation of queue that is backed by DoublyLinkedList """
class Queue:

    def __init__(self):
        self.container = DoublyLinkedList()

    def push(self, value):
        """ Push the value to the queue """
        self.container.add(value)

    def pop(self):
        """ Remove the topmost element from the queue and return it """
        return self.container.removeLast()

    def size(self):
        """ Returns the size of the queue """
        return self.container.size()

    def isEmpty(self):
        """ Tells if the queue is empty """
        return self.container.size() == 0

    def peek(self):
        """ Returns the topmost element without removing it """
        return self.container.getLast()

""" Implementation of dequeue that is backed by DoublyLinkedList """
class Dequeue:

    def __init__(self):
        self.container = DoublyLinkedList()

    def addFirst(self, value):
        """ Adds element to the beginning of the dequeue """
        self.container.add(value, 0)

    def addLast(self, value):
        """ Appends element to the end of the dequeue """
        self.container.add(value)

    def removeFirst(self):
        """ Removes the first element of the dequeue and returns it """
        return self.container.removeFirst()

    def removeLast(self):
        """ Removes the last element of the dequeue and returns it """
        return self.container.removeLast()

    def size(self):
        """ Returns the size of the dequeue """
        return self.container.size()

    def isEmpty(self):
        """ Tells whether the dequeue is empty """
        return self.container.size() == 0


""" Node class that is used in LL implementations """
class Node:

    def __init__(self):
        self.next = None
        self.previous = None

    def setValue(self, value):
        """ Sets the value of this node to the provided one """
        self.value = value

    def getValue(self):
        """ Returns the value """
        return self.value

    def setNext(self, next):
        """ Sets the NEXT reference to the one provided """
        self.next = next

    def getNext(self):
        """ Returns the reference to the NEXT element """
        return self.next

    def setPrevious(self, previous):
        """ Sets the PREVIOUS reference to the one provided """
        self.previous = previous

    def getPrevious(self):
        """ Returns the reference to the PREVIOUS element """
        return self.previous

""" List interface """
class List:

    def add(self, value):
        raise NotImplementedError

    def get(self, index):
        raise NotImplementedError

    def set(self, index, value):
        raise NotImplementedError

    def remove(self, index):
        raise NotImplementedError

    def removeFirst(self):
        raise NotImplementedError

    def removeLast(self):
        raise NotImplementedError

    def getLast(self):
        raise NotImplementedError

    def size(self):
        raise NotImplementedError

""" Cyclic LinkedList """
class CyclicLinkedList(List):

    def __init__(self):
        self.head = None
        self.tail = None

""" Doubly LinkedList """
class DoublyLinkedList(List):

    def __init__(self):
        self.head = None
        self.tail = None

    def add(self, value, index = None):
        """ Adds element to the list. Appends if index is not specified

        Arguments:
        value -- value to add
        index -- if not specified appends to the end of the list
                otherwise to the specified index. If the index is
                higher than the list size, element is appended to the
                end of the list.
        """
        node = Node()
        node.setValue(value)

        if index is not None:
            if self.head is None:
                self.head = node
                self.tail = node
                return
            elif index == 0:
                node.setNext(self.head)
                self.head = node
                return

            counter = 0
            current = self.head
            inserted = False
            while current is not None and not inserted:
                if (counter + 1) == index:
                    node.setNext(current.getNext())
                    node.setPrevious(current)
                    current.getNext().setPrevious(node)
                    current.setNext(node)
                    inserted = True
                else:
                    counter += 1
                    current = current.getNext()
            if not inserted:
                node.setPrevious(self.tail)
                self.tail.setNext(node)
                self.tail = node
        elif self.head != None:
            node.setPrevious(self.tail)
            self.tail.setNext(node)
            self.tail = node
        else:
            self.head = node
            self.tail = node

    def get(self, index):
        """ Returns the Nth element """
        if self.head == None:
            return None

        n = self.head
        counter = 0

        while n is not None:
            if counter == index:
                return n.getValue()
            else:
                n = n.next
                counter += 1

        return None

    def set(self, index, value):
        """ Sets the Nth element of the list to the specified value. If the index
            is higher than the list size nothing happens

        Arguments:
        index -- element index
        value -- element value
        returns -- boolean
        """
        if index == 0:
            if self.head is not None:
                counter = 0
                current = self.head
                while current is not None:
                    if counter == index:
                        current.setValue(value)
                        return True
                    else:
                        counter += 1
                return False
            else:
                node = Node()
                node.setValue(value)
                self.head = node
                self.tail = node
                return True

    def remove(self, index):
        """ Removes the Nth element and returns its value """
        if self.head == None:
            return None
        elif index == 0:
            if self.head.getNext() is not None:
                value = self.head.getValue()
                self.head = self.head.getNext()
                self.head.setPrevious(None)
                return value
            else:
                value = self.head.getValue()
                self.head = None
                self.tail = None
                return value
        else:
            counter = 1
            current = self.head.getNext()
            while current is not None:
                if counter == index:
                    current.getNext().setPrevious(current.getPrevious())
                    current.getPrevious().setNext(current.getNext())
                    value = current.getValue()
                    del current
                    return value
                else:
                    current = current.getNext()
                    counter += 1
            return None

    def removeFirst(self):
        """ Removes the first element of the list and returns its value """
        return self.remove(0)

    def removeLast(self):
        """ Removes the last element of the list and returns its value """
        if self.tail is not None:
            value = self.tail.getValue()
            self.tail = self.tail.getPrevious()
            if self.tail is not None:
                self.tail.setNext(None)
            else:
                self.head = None
            return value

    def getLast(self):
        """ Returns the value of the last element """
        return self.tail.getValue()

    def size(self):
        """ Returns the size of the list """
        if self.head == None:
            return 0
        else:
            counter = 1
            current = self.head.getNext()
            while current is not None:
                current = current.getNext()
                counter += 1
            return counter


""" Basic LinkedList """
class LinkedList(List):

    def __init__(self):
        self.value = None
        self.head = None
        self.tail = None

    def add(self, value, index = None):
        """ Adds element to the list

        Arguments:
        value -- value to add
        index -- if not specified appends to the end of the list
            otherwise to the specified index. If the index is
            higher than the list size, element is appended to the
            end of the list.
        """
        node = Node()
        node.setValue(value)

        if index is not None:
            if self.head is None:
                self.head = node
                self.tail = node
                return
            elif index == 0:
                node.setNext(self.head)
                self.head = node
                return

            counter = 0
            current = self.head
            inserted = False
            while current is not None and not inserted:
                if (counter + 1) == index:
                    node.setNext(current.getNext())
                    current.setNext(node)
                    inserted = True
                else:
                    counter += 1
                    current = current.getNext()
            if not inserted:
                self.tail.setNext(node)
                self.tail = node
        elif self.head != None:
            self.tail.setNext(node)
            self.tail = node
        else:
            self.head = node
            self.tail = node

    def get(self, index):
        """ Returns the Nth element """
        if self.head == None:
            return None

        n = self.head
        counter = 0
        
        while n is not None:
            if counter == index:
                return n.getValue()
            else:
                n = n.next
                counter += 1
        
        return None

    def set(self, index, value):
        """ Sets the Nth element of the list to the specified value. If the index
            is higher than the list size nothing happens

        Arguments:
        index -- element index
        value -- element value
        returns -- boolean
        """
        if index == 0:
            if self.head is not None:
                counter = 0
                current = self.head
                while current is not None:
                    if counter == index:
                        current.setValue(value)
                        return True
                    else:
                        counter += 1
                return False
            else:
                node = Node()
                node.setValue(value)
                self.head = node
                self.tail = node
                return True

    def remove(self, index):
        """ Removes the Nth element and returns its value """
        if self.head == None:
            return None
        elif index == 0:
            if self.head.getNext() is not None:
                value = self.head.getValue()
                self.head = self.head.getNext()
                return value
            else:
                value = self.head.getValue()
                self.head = None
                self.tail = None
                return value
        else:
            counter = 1
            current = self.head.getNext()
            previous = self.head
            while current is not None:
                if counter == index:
                    value = current.getValue()
                    previous.setNext(current.getNext())
                    del current
                    return value
                else:
                    previous = current
                    current = current.getNext()
                    counter += 1
            return None

    def removeFirst(self):
        """ Removes the first element of the list and returns its value """
        return self.remove(0)

    def removeLast(self):
        """ Removes the last element of the list and returns its value """
        if self.head is not None:
            value = self.tail.getValue()
            current = self.head
            while current.getNext() is not self.tail:
                current = current.getNext()
            self.tail = current
            self.tail.setNext(None)
            return value

    def getLast(self):
        """ Returns the value of the last element """
        return self.tail.getValue()

    def size(self):
        """ Returns the size of the list """
        if self.head == None:
            return 0
        else:
            counter = 1
            current = self.head.getNext()
            while current is not None:
                current = current.getNext()
                counter += 1
            return counter

    def printAll(self):
        n = self.head
        while n != None:
            print "{0} -> ".format(n.getValue()),
            n = n.getNext()
        print "None"


def success():
    print "SUCCESS"

if __name__ == "__main__":
    print "Running tests..."

    print "[0] Stack",
    stack = Stack()
    assert stack.size() == 0
    assert stack.isEmpty() == True
    stack.push("hello")
    stack.push(1)
    assert stack.size() == 2
    assert stack.isEmpty() == False
    assert stack.peek() == 1
    assert stack.pop() == 1
    assert stack.size() == 1
    assert stack.pop() == "hello"
    assert stack.size() == 0
    stack.push("boom")
    assert stack.isEmpty() == False
    stack.clear()
    assert stack.isEmpty() == True
    assert stack.size() == 0
    del stack
    success()

    print "[1] Node",
    node1 = Node()
    node2 = Node()
    assert node1.getNext() == None
    assert node1.getPrevious() == None
    node1.setValue(5)
    assert node1.getValue() == 5
    node2.setValue([])
    assert node2.getValue() == []
    node1.setNext(node2)
    node2.setPrevious(node1)
    assert node1.getNext() == node2
    assert node2.getPrevious() == node1
    del node1, node2
    success()

    print "[2] LinkedList",
    ll = LinkedList()
    assert ll.size() == 0
    ll.add("hi", 100)
    assert ll.size() == 1
    assert ll.get(0) == "hi"
    assert ll.remove(0) == "hi"
    ll.add("hello")
    assert ll.size() == 1
    for i in xrange(500):
        ll.add(i)
    assert ll.getLast() == 499
    assert ll.get(300) == 299
    assert ll.size() == 501
    assert ll.remove(300) == 299
    assert ll.size() == 500
    assert ll.get(300) == 300
    ll.add("boom", 300)
    assert ll.size() == 501
    assert ll.get(300) == "boom"
    assert ll.removeLast() == 499
    assert ll.getLast() == 498
    assert ll.size() == 500
    assert ll.get(0) == "hello"
    assert ll.removeFirst() == "hello"
    assert ll.get(0) == 0
    ll.add("why not", 0)
    assert ll.get(0) == "why not"
    del ll
    success()

    print "[3] DoublyLinkedList",
    dll = DoublyLinkedList()
    assert dll.size() == 0
    dll.add("hi", 100)
    assert dll.size() == 1
    assert dll.get(0) == "hi"
    assert dll.remove(0) == "hi"
    dll.add("hello")
    assert dll.size() == 1
    for i in xrange(500):
        dll.add(i)
    assert dll.getLast() == 499
    assert dll.get(300) == 299
    assert dll.size() == 501
    assert dll.remove(300) == 299
    assert dll.size() == 500
    assert dll.get(300) == 300
    dll.add("boom", 300)
    assert dll.size() == 501
    assert dll.get(300) == "boom"
    assert dll.removeLast() == 499
    assert dll.getLast() == 498
    assert dll.size() == 500
    assert dll.get(0) == "hello"
    assert dll.removeFirst() == "hello"
    assert dll.get(0) == 0
    dll.add("why not", 0)
    assert dll.get(0) == "why not"
    del dll
    success()

    print "[4] Queue",
    queue = Queue()
    queue.push("hello")
    assert queue.peek() == "hello"
    assert queue.size() == 1
    queue.push(1)
    queue.push(["boom"])
    assert queue.size() == 3
    assert queue.peek() == ["boom"]
    assert queue.pop() == ["boom"]
    assert queue.isEmpty() == False
    assert queue.pop() == 1
    assert queue.pop() == "hello"
    assert queue.size() == 0
    assert queue.isEmpty() == True
    del queue
    success()

    print "[5] Dequeue",
    dequeue = Dequeue()
    assert dequeue.size() == 0
    dequeue.addLast("hi")
    assert dequeue.size() == 1
    assert dequeue.removeFirst() == "hi"
    assert dequeue.size() == 0
    dequeue.addLast("hello1")
    dequeue.addLast("hello2")
    assert dequeue.removeFirst() == "hello1"
    dequeue.addFirst("hello0")
    assert dequeue.removeFirst() == "hello0"
    assert dequeue.isEmpty() == False
    assert dequeue.size() == 1
    assert dequeue.removeLast() == "hello2"
    assert dequeue.isEmpty() == True
    success()
